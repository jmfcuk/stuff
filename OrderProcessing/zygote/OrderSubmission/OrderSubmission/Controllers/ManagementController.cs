﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using OrderSubmission.Models;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace OrderSubmission.Controllers
{
    [Produces("application/json")]
    [Route("api/Management")]
    public class ManagementController : Controller
    {
        private IConfiguration _config;

        public ManagementController(IConfiguration config)
        {
            _config = config;
        }

        [Route("state/")]
        [EnableCors("CorsPolicy")]
        [HttpGet]
        public List<OrderStatePayload> State()
        {
            List<OrderStatePayload> states = new List<OrderStatePayload>();

            using (OrderProcessingContext ctx = 
                new OrderProcessingContext(_config))
            {
                foreach (var ocs in ctx.OrderCurrentState)
                {
                    OrderStatePayload osp = new OrderStatePayload()
                    {
                        OrderId = ocs.Id,
                        OrderState = ocs.OrderState,
                        OrderStateDescription = ocs.OrderStateDescription,
                        ProcessingState = ocs.ProcessingState,
                        ProcessingStateDescription = ocs.ProcessingStateDescription,
                        Comment = ocs.Comment
                    };

                    states.Add(osp);
                }
            }

            return states;
        }

        [Route("statetotal/")]
        [EnableCors("CorsPolicy")]
        [HttpGet]
        public List<OrderStateTotalPayload> Total()
        {
            List<OrderStateTotalPayload> totals = new List<OrderStateTotalPayload>();

            using (SqlConnection sc =
                new SqlConnection(_config.GetConnectionString("orderProcessing")))
            using (SqlCommand cmd =
                new SqlCommand
                ("SELECT OrderState, COUNT(OrderState) AS [Count] FROM OrderCurrentState GROUP BY OrderState", sc))
            {
                sc.Open();

                using (SqlDataReader sdr = cmd.ExecuteReader())
                {
                    while (sdr.Read())
                    {
                        OrderStateTotalPayload t = new OrderStateTotalPayload();
                        t.State = (OrderStateEnum)sdr["OrderState"];
                        t.Description = "";
                        t.Count = (int)sdr["Count"];

                        totals.Add(t);
                    }
                }
            }

            return totals;
        }
    }

    public class OrderStatePayload
    {
        public long OrderId { get; set; }
        public OrderStateEnum OrderState { get; set; }
        public string OrderStateDescription { get; set; }
        public OrderProcessingStateEnum ProcessingState { get; set; }
        public string ProcessingStateDescription { get; set; }
        public string Comment { get; set; }
    }

    public class OrderStateTotalPayload
    {
        public OrderStateEnum State { get; set; }
        public string Description { get; set; }
        public long Count { get; set; }
    }
}

