﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using OrderSubmission.ServiceBroker;
using System.Xml.Linq;

namespace OrderSubmission.Controllers
{
    [Produces("application/json")]
    [Route("api/submit")]
    public class SubmitController : Controller
    {
		private IConfiguration _config;

		public SubmitController(IConfiguration config)
		{
			_config = config;
		}

        [EnableCors("CorsPolicy")]
        [HttpPost]
        public string Submit([FromBody] XDocument order)
        {
			string schemaDir =
				_config.GetValue<string>("orderSchemaDir");

			bool b = OrderValidator.Validate(order, schemaDir);

			if (b)
				new ServiceBrokerClient(_config)
					.SubmitOrder(order.ToString());

			return GetDummyResponse();
		}

        private string GetDummyResponse()
		{
			return System.IO.File.ReadAllText(
                @"C:\_dev\hn\OrderProcessing\zygote\OrderSubmission\OrderSubmission\data\order-response.xml");
		}
    }
}

