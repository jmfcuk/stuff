﻿using System;

namespace OrderSubmission.Models
{
    public partial class OrderCurrentState
    {
        public long Id { get; set; }
        public long OrderId { get; set; }
        public OrderStateEnum OrderState { get; set; }
        public string OrderStateDescription { get; set; }
        public OrderProcessingStateEnum ProcessingState { get; set; }
        public string ProcessingStateDescription { get; set; }
        public string Xml { get; set; }
        public DateTime Created { get; set; }
        public DateTime Updated { get; set; }
        public string Comment { get; set; }
        public bool Deleted { get; set; }
    }
    
    public enum OrderStateEnum
    {
        Unknown = -999,
        Pending = 0,
        Importing = 1,
        Imported = 2,
        Assigning = 3,
        Assigned = 4,
        Picking = 5,
        Picked = 6,
        Packing = 7,
        Packed = 8,
        Dispatching = 9,
        Dispatched = 10,
        Refunding = 11,
        Refunded = 12,
        Cancelling = 13,
        Cancelled = 14,
        Complete = 15,
    }

    public enum OrderProcessingStateEnum
    {
        Unknown = -999,
        Pending = 0,
        Processing = 1,
        Success = 2,
        Warning = 3,
        Error = 4,
    }
}
