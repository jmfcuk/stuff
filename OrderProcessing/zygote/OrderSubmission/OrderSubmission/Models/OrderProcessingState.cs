﻿using System;
using System.Collections.Generic;

namespace OrderSubmission.Models
{
    public partial class OrderProcessingState
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int Value { get; set; }
    }
}
