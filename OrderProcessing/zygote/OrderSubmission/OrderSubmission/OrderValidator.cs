﻿using System;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;

namespace OrderSubmission
{
	public static class OrderValidator
	{
		public static bool Validate(XDocument order,
			string schemaDir)
		 {
			bool b = true;

			try
			{
				XmlSchemaSet schemas = new XmlSchemaSet();

				schemas.Add("",
					XmlReader
					.Create(
						@"file://C:\Projects\tfs\John%20Sandbox\OrderProcessing\zygote\OrderSubmission\OrderSubmission\schema\Common.xsd"));

				schemas.Add("",
					XmlReader
					.Create(
						@"file://C:\Projects\tfs\John%20Sandbox\OrderProcessing\zygote\OrderSubmission\OrderSubmission\schema\PushSalesOrderRequest.xsd"));

				order.Validate(schemas, (o, e) =>
				{
					var vo = o;
					var ex = e;
					string s = ex.Message;
				});
			}
			catch (Exception ex)
			{
				string s = ex.Message;
			}

			return b;
		}
	}
}
