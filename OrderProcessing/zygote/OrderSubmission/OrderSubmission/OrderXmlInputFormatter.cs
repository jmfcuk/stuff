﻿using Microsoft.AspNetCore.Mvc.Formatters;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace OrderSubmission
{
	public class OrderXmlInputFormatter : InputFormatter
	{
		public OrderXmlInputFormatter()
		{
			SupportedMediaTypes.Add("application/xml");
			SupportedMediaTypes.Add("text/xml");
		}

		public override bool CanRead(InputFormatterContext context)
		{
			bool b = false;

			if (context == null)
				throw new ArgumentNullException(nameof(context));

			if (SupportedMediaTypes.Contains(
					context.HttpContext.Request.ContentType))
				b = true;

			return b;
		}

		protected override bool CanReadType(Type type)
		{
			if (type.IsAssignableFrom(typeof(XDocument)))
				return true;

			return base.CanReadType(type);
		}

		public override async Task<InputFormatterResult> ReadRequestBodyAsync(InputFormatterContext context)
		{
			if (context == null)
				throw new ArgumentNullException(nameof(context));

			var contentType =
				context.HttpContext.Request.ContentType;

			if (!string.IsNullOrWhiteSpace(contentType) &&
				SupportedMediaTypes.Contains(contentType))
			{
				var xd = 
						await XDocument.LoadAsync(
							context.HttpContext.Request.Body, 
							LoadOptions.None, 
							CancellationToken.None);

					return await InputFormatterResult
						.SuccessAsync(xd);
			}

			return await InputFormatterResult.FailureAsync();
		}
	}
}


