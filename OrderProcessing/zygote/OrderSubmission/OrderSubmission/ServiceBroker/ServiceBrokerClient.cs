﻿using Microsoft.Extensions.Configuration;
using ServiceBrokerInterface;
using System;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace OrderSubmission.ServiceBroker
{
	public class ServiceBrokerClient
	{
		private static IConfiguration _config;

		public ServiceBrokerClient(IConfiguration config)
		{
			_config = config;
		}

		public bool SubmitOrder(string order)
		{
			bool b = false;

			string cs = _config.GetConnectionString("orderProcessing");

			using (SqlConnection sc = new SqlConnection(cs))
			{
				sc.Open();

				using (SqlTransaction tran = sc.BeginTransaction())
				{
					try
					{
						SBService svc =
							new SBService("OrderSubmissionInitiatorService", sc, tran);

						svc.FetchSize = 1;

						SBConversation conversation = svc.BeginDialog(
											"OrderSubmissionTargetService",
											null,
											 "OrderContract",
											TimeSpan.FromMinutes(5),
											false,
											sc,
											tran);

						using (MemoryStream ms = new MemoryStream())
						{
							using (StreamWriter sw = new StreamWriter(ms, Encoding.Unicode))
							{
								sw.Write(order);
								sw.Flush();
								ms.Position = 0;

								SBMessage msg = new SBMessage("OrderMessage", ms);

								conversation.Send(msg, sc, tran);

								conversation.EndConversation(sc, tran);

								tran.Commit();
							}
						}
					}
					catch (Exception ex)
					{
						tran.Rollback();

						throw;
					}
				}
			}

			return b;
		}
	}
}

/*
DECLARE @handle UNIQUEIDENTIFIER;
WHILE (SELECT COUNT(*) FROM [OrderProcessing].[dbo].[OrderSubmissionTargetQ]) > 0
BEGIN
	RECEIVE TOP (1) @handle = conversation_handle FROM [OrderProcessing].[dbo].[OrderSubmissionTargetQ];
	END CONVERSATION @handle WITH CLEANUP
END
*/
