﻿
namespace ActorMessageTypes
{
	public abstract class OrderActorMessage
	{
		protected const long INVALID_ID = -1;

		public long Id { get; }

		public OrderActorMessage()
		{
			Id = INVALID_ID;
		}

		public OrderActorMessage(long id)
		{
			Id = id;
		}
	}

	public abstract class ActorXmlMessage : OrderActorMessage
	{
		public string Xml { get; }

		public ActorXmlMessage(long id, string xml)
			: base(id)
		{
			Xml = xml;
		}
	}

	public class OrderImportMessage : OrderActorMessage
	{
		public OrderImportMessage() { }

		public OrderImportMessage(long id)
			: base(id)
		{ }
	}

	public class OrderActorResponseMessage : OrderActorMessage
	{
		public string Comment { get; private set; }

		public OrderActorResponseMessage(long id, 
										 string response)
			: base(id)
		{
			Comment = response;
		}
	}
}


