﻿
namespace Sandbox.Actors.Messages
{
	public abstract class OrderActorMessage
	{
		protected const long INVALID_ID = -1;

		public long Id { get; }

		public OrderActorMessage()
		{
			Id = INVALID_ID;
		}

		public OrderActorMessage(long id)
		{
			Id = id;
		}
	}

	public abstract class OrderActorXmlMessage : OrderActorMessage
	{
		public string Xml { get; }

		public OrderActorXmlMessage(long id, string xml)
			: base(id)
		{
			Xml = xml;
		}
	}

	public class OrderPendingMessage : OrderActorMessage
	{
		//public OrderPendingMessage() { }

		public OrderPendingMessage(long id)
			: base(id)
		{ }
	}

	public class OrderImportMessage : OrderActorMessage
	{
		public OrderImportMessage(long id)
			: base(id)
		{ }
	}

	public class OrderAssignMessage : OrderActorMessage
	{
		public OrderAssignMessage() { }

		public OrderAssignMessage(long id)
			: base(id)
		{ }
	}

	public class OrderPickMessage : OrderActorMessage
	{
		public OrderPickMessage() { }

		public OrderPickMessage(long id)
			: base(id)
		{ }
	}

	public class OrderPackMessage : OrderActorMessage
	{
		public OrderPackMessage() { }

		public OrderPackMessage(long id)
			: base(id)
		{ }
	}

	public class OrderDispatchMessage : OrderActorMessage
	{
		public OrderDispatchMessage() { }

		public OrderDispatchMessage(long id)
			: base(id)
		{ }
	}

	public class OrderRefundMessage : OrderActorMessage
	{
		public OrderRefundMessage() { }

		public OrderRefundMessage(long id)
			: base(id)
		{ }
	}

	public class OrderCancelMessage : OrderActorMessage
	{
		public OrderCancelMessage() { }

		public OrderCancelMessage(long id)
			: base(id)
		{ }
	}

	public class OrderCompleteMessage : OrderActorMessage
	{
		public OrderCompleteMessage(long id)
			: base(id)
		{ }
	}

	public class OrderResponseMessage : OrderActorMessage
	{
		public OrderStateEnum OrderState { get; }
		public OrderProcessingStateEnum ProcessingState { get; }
		public string Comment { get; private set; }

		public OrderResponseMessage(long id,
								    OrderStateEnum orderState,
								    OrderProcessingStateEnum processingState,
								    string comment = null)
			: base(id)
		{
			OrderState = orderState;
			ProcessingState = processingState;
			Comment = comment;
		}
	}
}


