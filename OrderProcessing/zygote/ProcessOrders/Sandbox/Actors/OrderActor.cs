﻿using Akka.Actor;
using Akka.Event;
using Sandbox.OfsOrderState;
using System;
using System.Data.SqlClient;

namespace Sandbox.Actors
{
	public abstract class OrderActor : ReceiveActor, ILogReceive
	{
		protected readonly ILoggingAdapter logger = Logging.GetLogger(Context);

		protected virtual string GetOrder(long id)
		{
			string order = OrderStateManager.Read(id);

			return order;
		}
		
		protected override SupervisorStrategy SupervisorStrategy()
		{
			return new OneForOneStrategy(
				maxNrOfRetries: 10,
				withinTimeRange: TimeSpan.FromMinutes(10),
				localOnlyDecider: ex =>
				{
					if (ex != null && ex is OrderActorException)
					{
						long id = (ex as OrderActorException).Id;

						//OrderManager.OrderState.Error(id, ex.Message);
						//OrderManager.ProcessingState.Error(id, ex.Message);

						return Directive.Restart;
					}

					return Akka.Actor.SupervisorStrategy.DefaultStrategy.Decider.Decide(ex);
				});
		}

	}
}

