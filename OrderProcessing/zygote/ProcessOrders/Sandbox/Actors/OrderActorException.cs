﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox.Actors
{
	public class OrderActorException : Exception
	{
		public long Id { get; set; }

		public OrderActorException(long id, string message, Exception innerEx = null)
			: base(message, innerEx)
		{
			Id = id;
		}
	}
}

