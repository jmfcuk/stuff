﻿using Akka.Actor;
using Akka.Routing;
using Sandbox.Actors.Messages;
using Sandbox.OfsOrderState;
using System.Configuration;
using System.Threading.Tasks;

namespace Sandbox.Actors
{
	/*
	OrderPendingMessage
	OrderImportMessage
	OrderAssignMessage
	OrderPickMessage
	OrderPackMessage
	OrderDispatchMessage
	OrderRefundMessage
	OrderCancelMessage
	OrderCompleteMessage

	OrderResponseMessage
	*/
	public class OrderCommanderActor : ReceiveActor
	{
		IActorRef _orderProcessingActors = null;

		public OrderCommanderActor()
		{
			Init();

			Receive<OrderPendingMessage>(msg =>
			{
				OrderPendingMessage opm = msg;
				
				_orderProcessingActors.Tell(
					new OrderImportMessage(msg.Id));
			});

			Receive<OrderResponseMessage>(msg =>
			{
				OrderResponseMessage orm = msg;

				HandleOrderResponse(orm);
			});
		}

		private void HandleOrderResponse(OrderResponseMessage orm)
		{
			UpdateState(orm.Id, orm.OrderState,
						orm.ProcessingState,
						orm.Comment);

			if (orm.OrderState == OrderStateEnum.Imported &&
			    orm.ProcessingState == OrderProcessingStateEnum.Success)
			{
				_orderProcessingActors.Tell(
					new OrderAssignMessage(orm.Id));
			}
			else if(orm.OrderState == OrderStateEnum.Assigned &&
				orm.ProcessingState == OrderProcessingStateEnum.Success)
			{
				_orderProcessingActors.Tell(
					new OrderPickMessage(orm.Id));
			}
			else if (orm.OrderState == OrderStateEnum.Picked &&
					 orm.ProcessingState == OrderProcessingStateEnum.Success)
			{
				_orderProcessingActors.Tell(
					new OrderPackMessage(orm.Id));
			}
			else if (orm.OrderState == OrderStateEnum.Packed &&
					 orm.ProcessingState == OrderProcessingStateEnum.Success)
			{
				_orderProcessingActors.Tell(
					new OrderDispatchMessage(orm.Id));
			}
			else if (orm.OrderState == OrderStateEnum.Dispatched &&
					 orm.ProcessingState == OrderProcessingStateEnum.Success)
			{
				UpdateState(orm.Id,
							OrderStateEnum.Complete,
							OrderProcessingStateEnum.Success);
			}
		}

		private void Init()
		{
			int orderProcessingActorsCount =
					int.Parse(
						ConfigurationManager.AppSettings["orderImportCommanderCount"]);

			_orderProcessingActors =
						Context.ActorOf(
							Props.Create<OrderProcessingActor>()
							.WithRouter(
								new RoundRobinPool(orderProcessingActorsCount)));
		}

		private void UpdateState(long id, OrderStateEnum orderState,
									OrderProcessingStateEnum processingState,
									string comment = null)
		{
			OrderStateManager.UpdateState
				.OrderProcessingState(
									id, orderState,
									processingState,
									comment);
		}
	}
}
