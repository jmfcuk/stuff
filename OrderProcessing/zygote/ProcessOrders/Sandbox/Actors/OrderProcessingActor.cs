﻿using Akka.Actor;
using Sandbox.Actors.Messages;
using System;
using System.Threading;

namespace Sandbox.Actors
{
	public class OrderProcessingActor : OrderActor
	{
		public OrderProcessingActor() 
		{
			Receive<OrderImportMessage>(msg =>
			{
				OrderImportMessage oim = msg;

				Sender.Tell(
					new OrderResponseMessage(
						oim.Id,
						OrderStateEnum.Importing,
						OrderProcessingStateEnum.Processing));

				RandomWait();

				Sender.Tell(
					new OrderResponseMessage(
						oim.Id,
						OrderStateEnum.Imported,
						OrderProcessingStateEnum.Success));
			});

			Receive<OrderAssignMessage>(msg =>
			{
				OrderAssignMessage oim = msg;

				Sender.Tell(
					new OrderResponseMessage(
						oim.Id,
						OrderStateEnum.Assigning,
						OrderProcessingStateEnum.Processing));

				RandomWait();

				Sender.Tell(
					new OrderResponseMessage(
						oim.Id,
						OrderStateEnum.Assigned,
						OrderProcessingStateEnum.Success));
			});

			Receive<OrderPickMessage>(msg =>
			{
				OrderPickMessage opm = msg;

				Sender.Tell(
					new OrderResponseMessage(
						opm.Id,
						OrderStateEnum.Picking,
						OrderProcessingStateEnum.Processing));

				RandomWait();

				Sender.Tell(
					new OrderResponseMessage(
						opm.Id,
						OrderStateEnum.Picked,
						OrderProcessingStateEnum.Success));
			});

			Receive<OrderPackMessage>(msg =>
			{
				OrderPackMessage odm = msg;

				Sender.Tell(
					new OrderResponseMessage(
						odm.Id,
						OrderStateEnum.Packing,
						OrderProcessingStateEnum.Processing));

				RandomWait();

				Sender.Tell(
					new OrderResponseMessage(
						odm.Id,
						OrderStateEnum.Packed,
						OrderProcessingStateEnum.Success));
			});

			Receive<OrderDispatchMessage>(msg =>
			{
				OrderDispatchMessage odm = msg;

				Sender.Tell(
					new OrderResponseMessage(
						odm.Id,
						OrderStateEnum.Dispatching,
						OrderProcessingStateEnum.Processing));

				RandomWait();

				Sender.Tell(
					new OrderResponseMessage(
						odm.Id,
						OrderStateEnum.Dispatched,
						OrderProcessingStateEnum.Success));
			});
		}
		
		private void RandomWait()
		{
			Random rnd = new Random();
			int i = (rnd.Next(2, 5)) * 1000;

			logger.Info("Pause for {i} seconds");

			Thread.Sleep(i);
		}
	}
}
