﻿using System.Xml.Linq;

namespace Sandbox
{
	static class OrderParse
	{
		public static string RequestId(string orderXml)
		{
			return XElement.Parse(orderXml).Element("RequestId").Value;
		}

		public static string CustomerId(string orderXml)
		{
			return XElement.Parse(orderXml)
					.Element("PushSalesOrderRequestContainer")
					.Element("Order")
					.Element("MagentoCustomerIncrementId")
					.Value;
		}

		public static int OrderId(string orderXml)
		{
			return 
				int.Parse(XElement.Parse(orderXml)
				.Element("PushSalesOrderRequestContainer")
				.Element("Order")
				.Element("OrderId").Value);
		}

		public static string Loyalty(string orderXml)
		{
			string xml =
			 XElement.Parse(orderXml)
				.Element("PushSalesOrderRequestContainer")
				.Element("Order")
				.Element("Loyalty")
				.ToString();

			return xml;
		}

		public static string Items(string orderXml)
		{
			string xml =
				XElement.Parse(orderXml)
					.Element("PushSalesOrderRequestContainer")
					.Element("Order")
					.Element("ItemArray")
					.ToString();

			return xml;
		}

		public static XElement ItemsElement(string orderXml)
		{
			XElement xe =
				XElement.Parse(orderXml)
						.Element("PushSalesOrderRequestContainer")
						.Element("Order")
						.Element("ItemArray");

			return xe;
		}
	}
}



