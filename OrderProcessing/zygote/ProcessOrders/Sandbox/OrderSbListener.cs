﻿using Akka.Actor;
using Sandbox.Actors.Messages;
using Sandbox.OfsOrderState;
using System;
using System.Configuration;
using System.Data.SqlClient;
using System.Threading;
using System.Threading.Tasks;

namespace Sandbox
{
	public class OrderSbListener
	{
		private CancellationTokenSource _cancelSbListen =
			new CancellationTokenSource();
		
		public void Start(int sbListenerCount, IActorRef orderImportActors)
		{
			Console.WriteLine($"OrderSbListener::Start({sbListenerCount}, {orderImportActors}");

			for (int x = 0; x < sbListenerCount; x++)
			{
				Task.Factory.StartNew(() =>
				{
					ListenForOrders(orderImportActors, _cancelSbListen.Token);
				});
			}
		}

		public void Stop()
		{
			Console.WriteLine("OrderSbListener::Stop()");

			_cancelSbListen.Cancel();
		}
		
		private long SubmitFromSbToOfs(SqlConnection sc, int count = 1)
		{
			long id = OrderStateManager.INVALID_ID;

			using (SqlTransaction tran = sc.BeginTransaction())
			{
				try
				{
					string order = SqlServiceBroker.GetNextOrder(sc, tran);

					if (null != order)
						id = OrderStateManager.SubmitNew(order, sc, tran);

					tran.Commit();
				}
				catch (Exception e)
				{
					tran.Rollback();
					id = OrderStateManager.INVALID_ID;
				}
			}

			return id;
		}

		private void ListenForOrders(IActorRef orderCommanderActors, 
									 CancellationToken ct)
		{
			Console.WriteLine("Entering OrderSbListener::ListenForOrders()");

			using (SqlConnection sc = new SqlConnection(
					ConfigurationManager.ConnectionStrings["orderProcessing"]
						.ConnectionString))
			{
				sc.Open();

				while (!ct.IsCancellationRequested)
				{
					long id = SubmitFromSbToOfs(sc);

					if (id != OrderStateManager.INVALID_ID)
						orderCommanderActors.Tell(new OrderPendingMessage(id));
				}
			}

			Console.WriteLine("Leaving OrderSbListener::ListenForOrders()");
		}
	}
}
 