namespace Sandbox.OfsOrderState
{
	using System;
	using System.ComponentModel.DataAnnotations.Schema;

	[Table("OrderCurrentState")]
    public partial class OrderCurrentStateEntity
	{
        public long Id { get; set; }

        public long OrderId { get; set; }

		public OrderStateEnum OrderState { get; set; }

		public string OrderStateDescription { get; set; }

		public OrderProcessingStateEnum ProcessingState { get; set; }

		public string ProcessingStateDescription { get; set; }

		public string Xml { get; set; }
		        
		[DatabaseGenerated(DatabaseGeneratedOption.Computed)]
		public DateTime? Created { get; set; }

		[DatabaseGenerated(DatabaseGeneratedOption.Computed)]
		public DateTime? Updated { get; set; }

        public string Comment { get; set; }

        public bool Deleted { get; set; }
    }
}
