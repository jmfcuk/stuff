using System.Data.Entity;
using System.Data.SqlClient;

namespace Sandbox.OfsOrderState
{
	public partial class OrderStateContext : DbContext
	{
		public OrderStateContext()
			: base("name=orderProcessing")
		{
		}

		public OrderStateContext(SqlConnection sc, SqlTransaction tran)
			: base(sc, false)
		{
			if (tran != null)
				Database.UseTransaction(tran);
		}

		public virtual DbSet<OrderCurrentStateEntity> OrderCurrentState { get; set; }
	}
}

