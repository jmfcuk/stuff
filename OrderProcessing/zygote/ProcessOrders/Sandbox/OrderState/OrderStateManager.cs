﻿using System.Data.SqlClient;
using System.Linq;

namespace Sandbox.OfsOrderState
{
	public static class OrderStateManager
	{
		public const int INVALID_ID = -1;

		private static OrderStateContext GetContext(SqlConnection sc, 
													   SqlTransaction tran)
		{
			OrderStateContext ctx = null;

			if (sc != null)
				ctx = new OrderStateContext(sc, tran);
			else
				ctx = new OrderStateContext();

			return ctx;
		}
		public static class UpdateState
		{
			public static void OrderState(long id, 
											OrderStateEnum state, 
											string comment = null, 
											SqlTransaction tran = null)
			{
				try
				{
					using (OrderStateContext db = new OrderStateContext())
					{
						if (tran != null)
							db.Database.UseTransaction(tran);

						OrderCurrentStateEntity order =
							db.OrderCurrentState.Where(o => o.Id == id).SingleOrDefault();

						if (order != null)
						{
							order.OrderState = state;
							order.Comment = string.IsNullOrWhiteSpace(comment) ?
								$"{order.OrderState} - {order.ProcessingState}" : comment;

							db.SaveChanges();
						}
					}
				}
				catch { }
			}

			public static void ProcessingState(long id, 
												 OrderProcessingStateEnum state, 
												 string comment = null, 
												 SqlTransaction tran = null)
			{
				try
				{
					using (OrderStateContext db = new OrderStateContext())
					{
						if (tran != null)
							db.Database.UseTransaction(tran);

						OrderCurrentStateEntity order =
							db.OrderCurrentState.Where(o => o.Id == id).SingleOrDefault();

						if (order != null)
						{
							order.ProcessingState = state;
							order.Comment = string.IsNullOrWhiteSpace(comment) ?
								$"{order.OrderState} - {order.ProcessingState}" : comment;
							db.SaveChanges();
						}
					}
				}
				catch { }
			}

			public static void OrderProcessingState(long id,
													OrderStateEnum orderState,
													OrderProcessingStateEnum processingState,
													string comment = null,
													SqlTransaction tran = null)
			{
				try
				{
					using (OrderStateContext db = new OrderStateContext())
					{
						if (tran != null)
							db.Database.UseTransaction(tran);

						OrderCurrentStateEntity order =
							db.OrderCurrentState.Where(o => o.Id == id).SingleOrDefault();

						if (order != null)
						{
							order.OrderState = orderState;
							order.ProcessingState = processingState;
							order.Comment = string.IsNullOrWhiteSpace(comment) ?
								$"{order.OrderState} - {order.ProcessingState}" : comment;
																				 ;
							db.SaveChanges();
						}
					}
				}
				catch { }
			}
		}

		public static long SubmitNew(string orderXml, 
									SqlConnection conn = null,
									SqlTransaction tran = null)
		{
			long id = INVALID_ID;

			int orderId = OrderParse.OrderId(orderXml);

			OrderCurrentStateEntity order = new OrderCurrentStateEntity();
			order.OrderId = orderId;
			order.Xml = orderXml;

			order.OrderState = OrderStateEnum.Pending;
			order.OrderStateDescription = $"{OrderStateEnum.Pending}";

			order.ProcessingState = OrderProcessingStateEnum.Pending;
			order.ProcessingStateDescription = $"{OrderProcessingStateEnum.Pending}";

			using (var db = GetContext(conn, tran))
			{
				db.OrderCurrentState.Add(order);
				db.SaveChanges();
			}

			id = order.Id;

			return id;
		}

		public static string Read(long id, bool includeDeleted = false)
		{
			string xml = null;

			using (var db = new OrderStateContext())
			{
				var order =
					db.OrderCurrentState.SingleOrDefault(q => q.Id == id && q.Deleted == includeDeleted);

				if (order != null)
				{
					xml = order.Xml;
				}
			}

			return xml;
		}

		public static void Delete(long id)
		{
			using (var db = new OrderStateContext())
			{
				var order =
					db.OrderCurrentState.SingleOrDefault(q => q.Id == id);

				if (order != null)
				{
					order.Deleted = true;
					db.SaveChanges();
				}
			}
		}

		public static void Purge(long id, SqlTransaction tran = null)
		{
			using (var db = new OrderStateContext())
			{
				var orders =
					db.OrderCurrentState.Where(q => q.Id == id && q.Deleted == true);

				foreach (var order in orders)
				{
					db.OrderCurrentState.Remove(order);
					db.SaveChanges();
				}
			}
		}

		public static void Purge(SqlTransaction tran = null)
		{
			using (var db = new OrderStateContext())
			{
				var orders =
					db.OrderCurrentState.Where(q => q.Deleted == true);

				foreach (var order in orders)
				{
					db.OrderCurrentState.Remove(order);
					db.SaveChanges();
				}
			}
		}
	}
}

public enum OrderStateEnum
{
	Unknown = -999,
	Pending = 0,
	Importing = 1,
	Imported = 2,
	Assigning = 3,
	Assigned = 4,
	Picking = 5,
	Picked = 6,
	Packing = 7,
	Packed = 8,
	Dispatching = 9,
	Dispatched = 10,
	Refunding = 11,
	Refunded = 12,
	Cancelling = 13,
	Cancelled = 14,
	Complete = 15,

}

public enum OrderProcessingStateEnum
{
	Unknown = -999,
	Pending = 0,
	Processing = 1,
	Success = 2,
	Warning = 3,
	Error = 4,
}


