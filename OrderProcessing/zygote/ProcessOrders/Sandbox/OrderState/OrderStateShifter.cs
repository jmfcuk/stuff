﻿using System.Collections.Generic;

namespace Sandbox.OfsOrderState
{
	public static class OrderStateShifter
	{
		public static readonly NextOrderState Next =
			new NextOrderState();
	}

	public class NextOrderState :
		Dictionary<OrderStateEnum, OrderStateEnum>
	{
		public NextOrderState()
			: base()
		{
			Add(OrderStateEnum.Pending, OrderStateEnum.Importing);
			Add(OrderStateEnum.Importing, OrderStateEnum.Imported);
			Add(OrderStateEnum.Imported, OrderStateEnum.Assigning);
			Add(OrderStateEnum.Assigning, OrderStateEnum.Assigned);
			Add(OrderStateEnum.Assigned, OrderStateEnum.Picking);
			Add(OrderStateEnum.Picking, OrderStateEnum.Picked);
			Add(OrderStateEnum.Picked, OrderStateEnum.Packing);
			Add(OrderStateEnum.Packing, OrderStateEnum.Packed);
			Add(OrderStateEnum.Packed, OrderStateEnum.Dispatching);
			Add(OrderStateEnum.Dispatched, OrderStateEnum.Complete);

			Add(OrderStateEnum.Refunding, OrderStateEnum.Refunded);
			Add(OrderStateEnum.Cancelling, OrderStateEnum.Cancelled);
		}
	}
}
