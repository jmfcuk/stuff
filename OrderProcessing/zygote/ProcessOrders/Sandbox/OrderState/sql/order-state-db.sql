﻿USE [master]
GO

/****** Object:  Database [OrderProcessing_John]    Script Date: 11/08/2017 16:50:28 ******/
CREATE DATABASE [OrderProcessing_John]
 CONTAINMENT = NONE
 ON  PRIMARY 
-- DON'T FORGET TO CHANGE THE PATH TO THE DATA AND LOG FILES!!!!
( NAME = N'OrderProcessing_John', FILENAME = N'S:\MSSQL\Data\OrderProcessing_John.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'OrderProcessing_John_log', FILENAME = N'T:\MSSQL\Log\OrderProcessing_John_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO

ALTER DATABASE [OrderProcessing_John] SET COMPATIBILITY_LEVEL = 130
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [OrderProcessing_John].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [OrderProcessing_John] SET ANSI_NULL_DEFAULT OFF 
GO

ALTER DATABASE [OrderProcessing_John] SET ANSI_NULLS OFF 
GO

ALTER DATABASE [OrderProcessing_John] SET ANSI_PADDING OFF 
GO

ALTER DATABASE [OrderProcessing_John] SET ANSI_WARNINGS OFF 
GO

ALTER DATABASE [OrderProcessing_John] SET ARITHABORT OFF 
GO

ALTER DATABASE [OrderProcessing_John] SET AUTO_CLOSE OFF 
GO

ALTER DATABASE [OrderProcessing_John] SET AUTO_SHRINK OFF 
GO

ALTER DATABASE [OrderProcessing_John] SET AUTO_UPDATE_STATISTICS ON 
GO

ALTER DATABASE [OrderProcessing_John] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO

ALTER DATABASE [OrderProcessing_John] SET CURSOR_DEFAULT  GLOBAL 
GO

ALTER DATABASE [OrderProcessing_John] SET CONCAT_NULL_YIELDS_NULL OFF 
GO

ALTER DATABASE [OrderProcessing_John] SET NUMERIC_ROUNDABORT OFF 
GO

ALTER DATABASE [OrderProcessing_John] SET QUOTED_IDENTIFIER OFF 
GO

ALTER DATABASE [OrderProcessing_John] SET RECURSIVE_TRIGGERS OFF 
GO

ALTER DATABASE [OrderProcessing_John] SET ENABLE_BROKER 
GO

ALTER DATABASE [OrderProcessing_John] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO

ALTER DATABASE [OrderProcessing_John] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO

ALTER DATABASE [OrderProcessing_John] SET TRUSTWORTHY OFF 
GO

ALTER DATABASE [OrderProcessing_John] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO

ALTER DATABASE [OrderProcessing_John] SET PARAMETERIZATION SIMPLE 
GO

ALTER DATABASE [OrderProcessing_John] SET READ_COMMITTED_SNAPSHOT OFF 
GO

ALTER DATABASE [OrderProcessing_John] SET HONOR_BROKER_PRIORITY ON 
GO

ALTER DATABASE [OrderProcessing_John] SET RECOVERY FULL 
GO

ALTER DATABASE [OrderProcessing_John] SET  MULTI_USER 
GO

ALTER DATABASE [OrderProcessing_John] SET PAGE_VERIFY CHECKSUM  
GO

ALTER DATABASE [OrderProcessing_John] SET DB_CHAINING OFF 
GO

ALTER DATABASE [OrderProcessing_John] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO

ALTER DATABASE [OrderProcessing_John] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO

ALTER DATABASE [OrderProcessing_John] SET DELAYED_DURABILITY = DISABLED 
GO

ALTER DATABASE [OrderProcessing_John] SET QUERY_STORE = OFF
GO

USE [OrderProcessing_John]
GO

ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO

ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO

ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO

ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO

ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO

ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO

ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO

ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO

ALTER DATABASE [OrderProcessing_John] SET  READ_WRITE 
GO


/* ********************************************************************************** */


USE [OrderProcessing_John]
GO
/****** Object:  Table [dbo].[OrderState]    Script Date: 09/08/2017 18:11:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[OrderState](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Value] [int] NOT NULL,
 CONSTRAINT [PK_OrderState] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[OrderProcessingState]    Script Date: 09/08/2017 18:10:47 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[OrderProcessingState](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Value] [int] NOT NULL,
 CONSTRAINT [PK_OrderProcessingState] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


/****** Object:  Table [dbo].[OrderXmlQueue]    Script Date: 09/08/2017 18:11:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[OrderCurrentState](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[OrderId] [bigint] NOT NULL,
	[OrderState] [int] NOT NULL,
	[OrderStateDescription] [nvarchar](max) NOT NULL,
	[ProcessingState] [int] NOT NULL,
	[ProcessingStateDescription] [nvarchar](max) NOT NULL,
	[Xml] [XML] NULL,
	[Created] [datetime] NOT NULL,
	[Updated] [datetime] NOT NULL,
	[Comment] [nvarchar](max) NULL,
	[Deleted] [bit] NOT NULL,
 CONSTRAINT [PK_OrderCurrentState] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

ALTER TABLE [dbo].[OrderCurrentState] ADD  CONSTRAINT [DF_OrderCurrentState_OrderState]  DEFAULT ((0)) FOR [OrderState]
GO

ALTER TABLE [dbo].[OrderCurrentState] ADD  CONSTRAINT [DF_OrderCurrentState_ProcessingState]  DEFAULT ((0)) FOR [ProcessingState]
GO

ALTER TABLE [dbo].[OrderCurrentState] ADD  CONSTRAINT [DF__OrderXmlQ__Deleted__2D27B809]  DEFAULT ((0)) FOR [Deleted]
GO

ALTER TABLE [dbo].[OrderCurrentState] ADD  CONSTRAINT [DF__OrderXmlQ__Created__2D27B809]  DEFAULT ((GETUTCDATE())) FOR [Created]
GO

ALTER TABLE [dbo].[OrderCurrentState] ADD  CONSTRAINT [DF__OrderXmlQ__Updated__2D27B809]  DEFAULT ((GETUTCDATE())) FOR [Updated]
GO



CREATE TRIGGER UpdateTrigger
ON [dbo].[OrderCurrentState]
AFTER UPDATE AS
  UPDATE [dbo].[OrderCurrentState]
  SET Updated = GETUTCDATE(),
  OrderStateDescription = (SELECT [Name] FROM [dbo].[OrderState] 
	  WHERE [Value] = (SELECT OrderState FROM Inserted)),
  ProcessingStateDescription = (SELECT [Name] FROM [dbo].[OrderProcessingState] 
	  WHERE [Value] = (SELECT ProcessingState FROM Inserted))
  WHERE Id IN (SELECT Id FROM Inserted);
GO


INSERT INTO [dbo].[OrderState] (Name, Value) VALUES(N'Unknown', -999);
INSERT INTO [dbo].[OrderState] (Name, Value) VALUES(N'Pending', 0);
INSERT INTO [dbo].[OrderState] (Name, Value) VALUES(N'Importing', 1);
INSERT INTO [dbo].[OrderState] (Name, Value) VALUES(N'Imported', 2);
INSERT INTO [dbo].[OrderState] (Name, Value) VALUES(N'Assigning', 3);
INSERT INTO [dbo].[OrderState] (Name, Value) VALUES(N'Assigned', 4);
INSERT INTO [dbo].[OrderState] (Name, Value) VALUES(N'Picking', 5);
INSERT INTO [dbo].[OrderState] (Name, Value) VALUES(N'Picked', 6);
INSERT INTO [dbo].[OrderState] (Name, Value) VALUES(N'Packing', 7);
INSERT INTO [dbo].[OrderState] (Name, Value) VALUES(N'Packed', 8);
INSERT INTO [dbo].[OrderState] (Name, Value) VALUES(N'Dispatching', 9);
INSERT INTO [dbo].[OrderState] (Name, Value) VALUES(N'Dispatched', 10);
INSERT INTO [dbo].[OrderState] (Name, Value) VALUES(N'Refunding', 11);
INSERT INTO [dbo].[OrderState] (Name, Value) VALUES(N'Refunded', 12);
INSERT INTO [dbo].[OrderState] (Name, Value) VALUES(N'Cancelling', 13);
INSERT INTO [dbo].[OrderState] (Name, Value) VALUES(N'Cancelled', 14);
INSERT INTO [dbo].[OrderState] (Name, Value) VALUES(N'Complete', 15);
GO

INSERT INTO [dbo].[OrderProcessingState] (Name, Value) VALUES(N'Unknown', -999);
INSERT INTO [dbo].[OrderProcessingState] (Name, Value) VALUES(N'Pending', 0);
INSERT INTO [dbo].[OrderProcessingState] (Name, Value) VALUES(N'Processing', 1);
INSERT INTO [dbo].[OrderProcessingState] (Name, Value) VALUES(N'Success', 2);
INSERT INTO [dbo].[OrderProcessingState] (Name, Value) VALUES(N'Warning', 3);
INSERT INTO [dbo].[OrderProcessingState] (Name, Value) VALUES(N'Error', 4);
GO

