﻿USE [OrderProcessing_John]
GO

/* Messages */
/****** Object:  MessageType [OrderMessage]    Script Date: 09/08/2017 18:05:45 ******/
CREATE MESSAGE TYPE [OrderMessage] VALIDATION = WELL_FORMED_XML
GO

/* Contracts */
/****** Object:  ServiceContract [OrderContract]    Script Date: 09/08/2017 18:06:00 ******/
CREATE CONTRACT [OrderContract] ([OrderMessage] SENT BY ANY)
GO

/* Queues */
/****** Object:  ServiceQueue [OrderSubmissionInitiatorQ]   Script Date: 09/08/2017 18:06:18 ******/
CREATE QUEUE [dbo].[OrderSubmissionInitiatorQ] WITH STATUS = ON , RETENTION = OFF , POISON_MESSAGE_HANDLING (STATUS = ON)  ON [PRIMARY] 
GO

/****** Object:  ServiceQueue [OrderSubmissionTargetQ]   Script Date: 09/08/2017 18:06:40 ******/
CREATE QUEUE [dbo].[OrderSubmissionTargetQ] WITH 
	STATUS = ON , 
	RETENTION = OFF
	ON [PRIMARY] 
GO

/****** Object:  ServiceQueue [OrderPendingInitiatorQ]   Script Date: 09/08/2017 18:06:18 ******/
--CREATE QUEUE [dbo].[OrderPendingInitiatorQ] WITH 
--	STATUS = ON , 
--	RETENTION = OFF , 
--	POISON_MESSAGE_HANDLING (STATUS = ON)  
--	ON [PRIMARY] 
--GO

--/****** Object:  ServiceQueue [OrderPendingTargetQ]   Script Date: 09/08/2017 18:06:40 ******/
--CREATE QUEUE [dbo].[OrderPendingTargetQ] WITH 
--	STATUS = ON , 
--	RETENTION = OFF , 
--	POISON_MESSAGE_HANDLING (STATUS = ON)
--	ON [PRIMARY] 
--GO

------/****** Object:  ServiceQueue [OrderImportInitiatorQ]    Script Date: 09/08/2017 18:06:18 ******/
------CREATE QUEUE [dbo].[OrderImportInitiatorQ] WITH STATUS = ON , RETENTION = OFF , POISON_MESSAGE_HANDLING (STATUS = ON)  ON [PRIMARY] 
------GO
------/****** Object:  ServiceQueue [OrderImportTargetQ]   Script Date: 09/08/2017 18:06:40 ******/
------CREATE QUEUE [dbo].[OrderImportTargetQ] WITH STATUS = ON , RETENTION = OFF , POISON_MESSAGE_HANDLING (STATUS = ON)  ON [PRIMARY] 
------GO

------/****** Object:  ServiceQueue [OrderAssignInitiatorQ]    Script Date: 09/08/2017 18:06:18 ******/
------CREATE QUEUE [dbo].[OrderAssignInitiatorQ] WITH STATUS = ON , RETENTION = OFF , POISON_MESSAGE_HANDLING (STATUS = ON)  ON [PRIMARY] 
------GO
------/****** Object:  ServiceQueue [OrderAssignTargetQ]   Script Date: 09/08/2017 18:06:40 ******/
------CREATE QUEUE [dbo].[OrderAssignTargetQ] WITH STATUS = ON , RETENTION = OFF , POISON_MESSAGE_HANDLING (STATUS = ON)  ON [PRIMARY] 
------GO

------/****** Object:  ServiceQueue [OrderDispatchInitiatorQ]    Script Date: 09/08/2017 18:06:18 ******/
------CREATE QUEUE [dbo].[OrderDispatchInitiatorQ] WITH STATUS = ON , RETENTION = OFF , POISON_MESSAGE_HANDLING (STATUS = ON)  ON [PRIMARY] 
------GO
------/****** Object:  ServiceQueue [OrderDispatchTargetQ]   Script Date: 09/08/2017 18:06:40 ******/
------CREATE QUEUE [dbo].[OrderDispatchTargetQ] WITH STATUS = ON , RETENTION = OFF , POISON_MESSAGE_HANDLING (STATUS = ON)  ON [PRIMARY] 
------GO

------/****** Object:  ServiceQueue [OrderDispatchInitiatorQ]    Script Date: 09/08/2017 18:06:18 ******/
------CREATE QUEUE [dbo].[OrderRefundInitiatorQ] WITH STATUS = ON , RETENTION = OFF , POISON_MESSAGE_HANDLING (STATUS = ON)  ON [PRIMARY] 
------GO
------/****** Object:  ServiceQueue [OrderDispatchTargetQ]   Script Date: 09/08/2017 18:06:40 ******/
------CREATE QUEUE [dbo].[OrderRefundTargetQ] WITH STATUS = ON , RETENTION = OFF , POISON_MESSAGE_HANDLING (STATUS = ON)  ON [PRIMARY] 
------GO

------/****** Object:  ServiceQueue [OrderDispatchInitiatorQ]    Script Date: 09/08/2017 18:06:18 ******/
------CREATE QUEUE [dbo].[OrderCancelInitiatorQ] WITH STATUS = ON , RETENTION = OFF , POISON_MESSAGE_HANDLING (STATUS = ON)  ON [PRIMARY] 
------GO
------/****** Object:  ServiceQueue [OrderDispatchTargetQ]   Script Date: 09/08/2017 18:06:40 ******/
------CREATE QUEUE [dbo].[OrderCancelTargetQ] WITH STATUS = ON , RETENTION = OFF , POISON_MESSAGE_HANDLING (STATUS = ON)  ON [PRIMARY] 
------GO

------/****** Object:  ServiceQueue [OrderDispatchInitiatorQ]    Script Date: 09/08/2017 18:06:18 ******/
------CREATE QUEUE [dbo].[OrderErrorInitiatorQ] WITH STATUS = ON , RETENTION = OFF , POISON_MESSAGE_HANDLING (STATUS = ON)  ON [PRIMARY] 
------GO
------/****** Object:  ServiceQueue [OrderDispatchTargetQ]   Script Date: 09/08/2017 18:06:40 ******/
------CREATE QUEUE [dbo].[OrderErrorTargetQ] WITH STATUS = ON , RETENTION = OFF , POISON_MESSAGE_HANDLING (STATUS = ON)  ON [PRIMARY] 
------GO

/* Services */

/****** Object:  BrokerService [OrderSubmissionInitiatorService]    Script Date: 09/08/2017 18:06:55 ******/
CREATE SERVICE [OrderSubmissionInitiatorService] 
ON QUEUE [dbo].[OrderSubmissionInitiatorQ] ([OrderContract])
GO
/****** Object:  BrokerService [OrderSubmissionTargetService]    Script Date: 09/08/2017 18:06:55 ******/
CREATE SERVICE [OrderSubmissionTargetService]  
ON QUEUE [dbo].[OrderSubmissionTargetQ] ([OrderContract])
GO

/****** Object:  BrokerService [OrderPendingInitiatorService]    Script Date: 09/08/2017 18:06:55 ******/
--CREATE SERVICE [OrderPendingInitiatorService]  ON QUEUE [dbo].[OrderPendingInitiatorQ] ([OrderContract])
--GO
--/****** Object:  BrokerService [OrderPendingTargetService]    Script Date: 09/08/2017 18:06:55 ******/
--CREATE SERVICE [OrderPendingTargetService]  ON QUEUE [dbo].[OrderPendingTargetQ] ([OrderContract])
--GO

------/****** Object:  BrokerService [OrderImportInitiatorService]    Script Date: 09/08/2017 18:06:55 ******/
------CREATE SERVICE [OrderImportInitiatorService]  ON QUEUE [dbo].[OrderImportInitiatorQ] ([OrderContract])
------GO
------/****** Object:  BrokerService [OrderImportTargetService]    Script Date: 09/08/2017 18:06:55 ******/
------CREATE SERVICE [OrderImportTargetService]  ON QUEUE [dbo].[OrderImportTargetQ] ([OrderContract])
------GO

------/****** Object:  BrokerService [OrderAssignInitiatorService]     Script Date: 09/08/2017 18:06:55 ******/
------CREATE SERVICE [OrderAssignInitiatorService]  ON QUEUE [dbo].[OrderAssignInitiatorQ] ([OrderContract])
------GO
------/****** Object:  BrokerService [OrderAssignTargetService]    Script Date: 09/08/2017 18:06:55 ******/
------CREATE SERVICE [OrderAssignTargetService]  ON QUEUE [dbo].[OrderAssignTargetQ] ([OrderContract])
------GO

------/****** Object:  BrokerService [OrderDispatchInitiatorService]    Script Date: 09/08/2017 18:06:55 ******/
------CREATE SERVICE [OrderDispatchInitiatorService]  ON QUEUE [dbo].[OrderDispatchInitiatorQ] ([OrderContract])
------GO
------/****** Object:  BrokerService [OrderDispatchTargetService]    Script Date: 09/08/2017 18:06:55 ******/
------CREATE SERVICE [OrderDispatchTargetService]  ON QUEUE [dbo].[OrderDispatchTargetQ] ([OrderContract])
------GO

------/****** Object:  BrokerService [OrderRefundInitiatorService]    Script Date: 09/08/2017 18:06:55 ******/
------CREATE SERVICE [OrderRefundInitiatorService]  ON QUEUE [dbo].[OrderRefundInitiatorQ] ([OrderContract])
------GO
------/****** Object:  BrokerService [OrderRefundTargetService]   Script Date: 09/08/2017 18:06:55 ******/
------CREATE SERVICE [OrderRefundTargetService]  ON QUEUE [dbo].[OrderRefundTargetQ] ([OrderContract])
------GO

------/****** Object:  BrokerService [OrderCancelInitiatorService]    Script Date: 09/08/2017 18:06:55 ******/
------CREATE SERVICE [OrderCancelInitiatorService]  ON QUEUE [dbo].[OrderCancelInitiatorQ] ([OrderContract])
------GO
------/****** Object:  BrokerService [OrderCancelTargetService]    Script Date: 09/08/2017 18:06:55 ******/
------CREATE SERVICE [OrderCancelTargetService]  ON QUEUE [dbo].[OrderCancelTargetQ] ([OrderContract])
------GO

------/****** Object:  BrokerService [OrderErrorInitiatorService]    Script Date: 09/08/2017 18:06:55 ******/
------CREATE SERVICE [OrderErrorInitiatorService]  ON QUEUE [dbo].[OrderErrorInitiatorQ] ([OrderContract])
------GO
------/****** Object:  BrokerService [OrderErrorTargetService]    Script Date: 09/08/2017 18:06:55 ******/
------CREATE SERVICE [OrderErrorTargetService]  ON QUEUE [dbo].[OrderErrorTargetQ] ([OrderContract])
------GO

CREATE PROCEDURE [dbo].[OnOrderSubmitted]
AS
  DECLARE @handle UNIQUEIDENTIFIER;
  DECLARE @msg XML;
  DECLARE @msgName sysname;

  WHILE (1=1)
  BEGIN

    BEGIN TRANSACTION;

    WAITFOR
    ( RECEIVE TOP(1)
        @handle = conversation_handle,
        @msg = message_body,
        @msgName = message_type_name
      FROM [dbo].[OrderSubmissionTargetQ]
    ), TIMEOUT 5000;

    IF (@@ROWCOUNT = 0)
    BEGIN
      ROLLBACK TRANSACTION;
      BREAK;
    END

    IF @msgName = N'OrderMessage'
		BEGIN
       		DECLARE @orderId int

			SET @orderId = 
				@msg.value('(//OrderId)[0]', 'int');

			INSERT INTO [dbo].[OrderCurrentState]
			([OrderId], [OrderState], [OrderStateDescription], 
			 [ProcessingState], [ProcessingStateDescription], 
			 [Xml], [Comment]) 
			VALUES (@orderId, 0, (SELECT [Name] FROM [dbo].[OrderState] 
									 WHERE [Value] = 0), 
			0, (SELECT [Name] FROM [dbo].[OrderProcessingState] 
						WHERE [Value] =0), @msg, N'No comment :-)');
	    END
    ELSE IF @msgName =
        N'http://schemas.microsoft.com/SQL/ServiceBroker/EndDialog'
    BEGIN
       END CONVERSATION @handle;
    END
    ELSE IF @msgName =
        N'http://schemas.microsoft.com/SQL/ServiceBroker/Error'
    BEGIN
       END CONVERSATION @handle;
    END
      
    COMMIT TRANSACTION;

  END
GO


--ALTER QUEUE [dbo].[OrderSubmissionTargetQ]
--    WITH ACTIVATION
--    ( STATUS = ON,
--      PROCEDURE_NAME = [dbo].[OnOrderSubmitted],
--      MAX_QUEUE_READERS = 10,
--      EXECUTE AS SELF
--    );
--GO

ALTER QUEUE [dbo].[OrderSubmissionTargetQ]
    WITH ACTIVATION
    ( STATUS = OFF
    );
GO

