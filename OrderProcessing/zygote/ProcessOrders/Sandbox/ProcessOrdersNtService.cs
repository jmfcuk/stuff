﻿using Akka.Actor;
using Akka.Routing;
using Sandbox.Actors;
using System.Configuration;

namespace Sandbox
{
	public class ProcessOrdersNtService
	{
		private const string _ACTOR_SYSTEM_NAME =
			"OrderProcessingActorSystem";

		private ActorSystem _actorSystem;
		private IActorRef _orderCommanderActors;

		private OrderSbListener _orderListener = 
			new OrderSbListener();

		public void Start()
		{
			StartActorSystem();
			StartOrderListener();		
		}

		public void Stop()
		{
			StopOrderListener();
			StopActorSystem();
		}
		
		private void StartActorSystem()
		{
			_actorSystem = ActorSystem.Create(_ACTOR_SYSTEM_NAME);

			int orderImportCommanderCount =
						int.Parse(
							ConfigurationManager.AppSettings["orderImportCommanderCount"]);

			_orderCommanderActors =
						_actorSystem.ActorOf(Props.Create<OrderCommanderActor>()
							.WithRouter(new RoundRobinPool(orderImportCommanderCount)));
		}

		private void StopActorSystem()
		{
			_actorSystem.Terminate().Wait();
		}

		private void StartOrderListener()
		{
			int sbListenerCount =
				int.Parse(ConfigurationManager.AppSettings["orderSbListenerCount"]);
			
			_orderListener.Start(sbListenerCount, _orderCommanderActors);
		}

		private void StopOrderListener()
		{
			_orderListener.Stop();
		}
	}
}


