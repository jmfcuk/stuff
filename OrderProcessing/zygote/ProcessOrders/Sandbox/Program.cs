﻿using Topshelf;

namespace Sandbox
{
	class Program
	{
		private const string _SERVICE_NAME = "Process Orders Service";

		static void Main(string[] args)
		{
			HostFactory.Run(x =>
			{
				x.Service<ProcessOrdersNtService>(s =>
				{
					s.ConstructUsing(n => new ProcessOrdersNtService());
					s.WhenStarted(service => service.Start());
					s.WhenStopped(service => service.Stop());
				});

				x.SetServiceName(_SERVICE_NAME);
				x.RunAsPrompt();
			});
		}
	}
}


