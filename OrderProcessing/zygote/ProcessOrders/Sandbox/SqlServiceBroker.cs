﻿using ServiceBrokerInterface;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace Sandbox
{
	internal static class SqlServiceBroker
	{
		internal static string GetNextOrder(SqlConnection sc, SqlTransaction tran, int count = 1)
		{
			string order = null;

			SBService svc =
					new SBService("OrderSubmissionTargetService", sc, tran);

			svc.FetchSize = count;

			SBConversation c = svc.GetConversation(sc, tran);

			if (c != null)
			{
				SBMessage msg = c.Receive();

				if (msg.MessageType == 
					"http://schemas.microsoft.com/SQL/ServiceBroker/EndDialog")
					c.EndConversation(sc, tran);
				else if (msg.MessageType == 
					"http://schemas.microsoft.com/SQL/ServiceBroker/Error")
					c.EndConversation(sc, tran);
				else if (msg.MessageType == "OrderMessage")
				{
					using (StreamReader sr = new StreamReader(msg.Body, Encoding.UTF8))
					{
						order = sr.ReadToEnd();
					}
				}
			}

			return order;
		}
	}
}


