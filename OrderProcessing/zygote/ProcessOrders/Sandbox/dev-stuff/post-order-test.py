# !/usr/bin/env python
# -*- coding: utf8 -*-

import sys, getopt, time, threading, requests, queue, csv, json, codecs, timeit, uuid

lock = threading.Lock()

order_id = 1
group_order_id = 1

def inc_order_and_group_id():
    global order_id
    global group_order_id
    with lock:
        order_id += 1
        group_order_id += 1

def do_get(uri, headers):

    start = timeit.default_timer()
    
    resp = requests.get(uri, headers = headers)
    
    end = timeit.default_timer()
    
    duration = end - start
    
    return resp, duration

def do_post(uri, headers, body):

    start = timeit.default_timer()
    
    resp = requests.post(uri, headers = headers, data = body)
    
    end = timeit.default_timer()
    
    duration = end - start
    
    return resp, duration

def iterate_get(q, uri, thread_index, iterations, delay_per_iteration):
    
    headers = { 'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.101 Safari/537.36' }
    
    for iteration in range(iterations):
    
        print("running GET Thread {}, Iteration {}".format(thread_index, iteration))
        
        if delay_per_iteration > 0:
            time.sleep(delay_per_iteration)
        
        response, duration = do_get(uri, headers)

        q.put({ 'thread_index': thread_index, 'iteration': iteration, 'verb': 'get', 'uri': uri, 'request_headers': json.dumps(dict(headers)), 'request_body': "", 'status': response.status_code, 'response_headers': json.dumps(dict(response.headers)), 'response _content': str(response.content), 'duration': duration })
    
def iterate_post(q, uri, thread_index, iterations, delay_per_iteration):

    for iteration in range(iterations):
    
        print("running POST Thread {}, Iteration {}".format(thread_index, iteration))
        
        if delay_per_iteration > 0:
            time.sleep(delay_per_iteration)
            
        headers, body = get_post_request()
        response, duration = do_post(uri, headers, body)

        q.put({ 'thread_index': thread_index, 'iteration': iteration, 'verb': 'post', 'uri': uri, 'request_headers': json.dumps(dict(headers)), 'request_body': body, 'status': response.status_code, 'response_headers': json.dumps(dict(response.headers)), 'response _content': str(response.content), 'duration': duration })
   
def load_body_from_file(path):

    print("load_body {}".format(path))

    body = ''
    
    with open(path, 'r') as f:
        body = f.read()
    
    return body
       
def get_post_request(path = None):

    # { "content-type": "application/soap+xml" }
    headers = { 'content-type': 'text/xml', 
                'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.101 Safari/537.36' }
    
    body = ''
    
    if not path is None:
        body = load_body_from_file(path)
    else:        
        body = '''<PushSalesOrderRequest xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://uat.harveynichols.com/resources/Api/schema/PushSalesOrderRequest.xsd"><RequestId>{}</RequestId><RequesterCredentials /><PushSalesOrderRequestContainer><Order><OrderId>{}</OrderId><SalesChannel>UK_website</SalesChannel><CreationDate>2017-08-29T07:25:26+01:00</CreationDate><MagentoCustomerIncrementId>2000004673987</MagentoCustomerIncrementId><LoyaltyId xsi:nil="true" /><CustomerEmail>john.field@harveynichols.com</CustomerEmail><Gender xsi:nil="true" /><DateOfBirth>1999-11-04</DateOfBirth><CustomerSegmentCodeArray /><CouponCode xsi:nil="true" /><GiftMessage xsi:nil="true" /><IsHamper>false</IsHamper><GroupOrderId>{}</GroupOrderId><GroupOrderCount>1</GroupOrderCount><GroupOrderNumber>1</GroupOrderNumber><GroupOrderTotal>521</GroupOrderTotal><CurrencyCode>GBP</CurrencyCode><Delivery><DeliveryServiceName>Standard</DeliveryServiceName><Sku>714455</Sku><Consolidate>false</Consolidate><DeliveryDate xsi:nil="true" /></Delivery><BillingAddress><Name><Prefix>Mr</Prefix><FirstName>John</FirstName><Surname>Field</Surname></Name><Company xsi:nil="true" /><StreetArray><Street>28 St. Lukes Mews</Street></StreetArray><City>London</City><Region xsi:nil="true" /><Postcode>W11 1DG</Postcode><CountryId>GB</CountryId><Telephone>07506123456</Telephone></BillingAddress><DeliveryAddress><Name><Prefix>Mr</Prefix><FirstName>John</FirstName><Surname>Field</Surname></Name><Company xsi:nil="true" /><StreetArray><Street>28 St. Lukes Mews</Street></StreetArray><City>London</City><Region xsi:nil="true" /><Postcode>W11 1DG</Postcode><CountryId>GB</CountryId><Telephone>07506123456</Telephone></DeliveryAddress><ItemArray><Item><Sku>2233416</Sku><SellerId xsi:nil="true" /><OfferId /><Quantity>1</Quantity><Description>MA-1 dark green shell bomber jacket</Description><Totals><Unit><Price>120</Price><Discount>0</Discount><Tax>0</Tax><Total>120</Total></Unit><Row><Discount>0</Discount><Tax>0</Tax><Total>120</Total></Row></Totals></Item><Item><Sku>2298912</Sku><SellerId xsi:nil="true" /><OfferId /><Quantity>1</Quantity><Description>Beat blue denim jacket</Description><Totals><Unit><Price>275</Price><Discount>0</Discount><Tax>0</Tax><Total>275</Total></Unit><Row><Discount>0</Discount><Tax>0</Tax><Total>275</Total></Row></Totals></Item><Item><Sku>2299408</Sku><SellerId xsi:nil="true" /><OfferId /><Quantity>1</Quantity><Description>J45 indigo straight-leg jeans</Description><Totals><Unit><Price>120</Price><Discount>0</Discount><Tax>0</Tax><Total>120</Total></Unit><Row><Discount>0</Discount><Tax>0</Tax><Total>120</Total></Row></Totals></Item></ItemArray><Totals><Items><Discount>0</Discount><Tax>0</Tax><Total>515</Total></Items><Delivery><Discount>0</Discount><Tax>0</Tax><Total>6</Total></Delivery><Final><Discount>0</Discount><Tax>0</Tax><Total>521</Total></Final></Totals><Payment><Cybersource><Response>utf8=%E2%9C%93&amp;auth_cv_result=P&amp;req_locale=en&amp;decision_case_priority=3&amp;req_item_0_sku=2233416&amp;req_bill_to_surname=Field&amp;req_item_0_quantity=1&amp;req_item_1_name=Beat+blue+denim+jacket&amp;score_rcode=1&amp;auth_amount=521.00&amp;req_merchant_defined_data8=217&amp;auth_time=2017-08-29T062529Z&amp;decision_early_return_code=9999999&amp;req_item_2_name=J45+indigo+straight-leg+jeans+&amp;transaction_id=5039879279576078004105&amp;req_item_2_unit_price=120.00&amp;score_device_fingerprint_javascript_enabled=true&amp;score_ip_city=southwark&amp;req_override_custom_receipt_page=https%3A%2F%2Fuat.harveynichols.com%2F%2Fintegration%2Fampersand_cybersource_sop_ba%2Fcallback&amp;req_merchant_defined_data1=UK_website&amp;req_merchant_defined_data2=Standard+Delivery&amp;req_merchant_defined_data3=false&amp;req_merchant_defined_data4=General&amp;req_merchant_defined_data5=authorization&amp;auth_code=521&amp;req_ship_to_address_postal_code=W11+1DG&amp;req_bill_to_address_country=GB&amp;auth_cv_result_raw=1&amp;score_device_fingerprint_cookies_enabled=true&amp;score_suspicious_info=ANOM-OS%5ERISK-TB&amp;req_ship_to_address_city=London&amp;decision_rcode=1&amp;score_rmsg=Score+exceeds+threshold.+Score+%3D+83&amp;score_device_fingerprint_browser_language=en-US%2Cen%3Bq%3D0.8&amp;req_bill_to_address_line1=28+St+Lukes+Mews&amp;req_card_number=xxxxxxxxxxxx1111&amp;score_device_fingerprint_true_ipaddress=217.158.166.34&amp;signature=6LJHx9Rf%2BVdgUyfaYbqzZR0QscSR7nW4imYptkxx6xE%3D&amp;score_ip_routing_method=fixed&amp;req_item_1_unit_price=275.00&amp;request_token=Ahj77wSTETZgDll%2Fvq%2BJDkuHTKOsEBS4dMo6wc6IZcI4kQyaSZbpAdmVlAnJiJswByy%2F31fEgAAA4g4N&amp;req_amount=521.00&amp;payer_authentication_reason_code=100&amp;decision_velocity_info=GVEL-R5020%5EGVEL-R5021%5EGVEL-R5022%5EGVEL-R5026%5EGVEL-R5027%5EGVEL-R5033%5EGVEL-R5034&amp;req_item_2_quantity=1&amp;req_item_2_sku=2299408&amp;decision=ACCEPT&amp;req_ship_to_address_country=GB&amp;decision_return_code=1320000&amp;req_ship_to_phone=07506123456&amp;req_customer_ip_address=217.158.166.34&amp;signed_field_names=transaction_id%2Cdecision%2Creq_access_key%2Creq_profile_id%2Creq_transaction_uuid%2Creq_transaction_type%2Creq_reference_number%2Creq_amount%2Creq_currency%2Creq_line_item_count%2Creq_locale%2Creq_payment_method%2Creq_override_custom_receipt_page%2Creq_payment_token%2Creq_item_0_name%2Creq_item_0_quantity%2Creq_item_0_sku%2Creq_item_0_unit_price%2Creq_item_1_name%2Creq_item_1_quantity%2Creq_item_1_sku%2Creq_item_1_unit_price%2Creq_item_2_name%2Creq_item_2_quantity%2Creq_item_2_sku%2Creq_item_2_unit_price%2Creq_bill_to_forename%2Creq_bill_to_surname%2Creq_bill_to_email%2Creq_bill_to_phone%2Creq_bill_to_address_line1%2Creq_bill_to_address_city%2Creq_bill_to_address_state%2Creq_bill_to_address_country%2Creq_bill_to_address_postal_code%2Creq_ship_to_forename%2Creq_ship_to_surname%2Creq_ship_to_phone%2Creq_ship_to_address_line1%2Creq_ship_to_address_city%2Creq_ship_to_address_country%2Creq_ship_to_address_postal_code%2Creq_card_number%2Creq_card_type%2Creq_card_expiry_date%2Creq_customer_ip_address%2Creq_device_fingerprint_id%2Creq_merchant_defined_data1%2Creq_merchant_defined_data2%2Creq_merchant_defined_data3%2Creq_merchant_defined_data4%2Creq_merchant_defined_data5%2Creq_merchant_defined_data8%2Creq_merchant_defined_data10%2Creq_merchant_defined_data11%2Creq_merchant_defined_data12%2Creq_merchant_defined_data13%2Creq_merchant_defined_data14%2Cmessage%2Creason_code%2Cauth_avs_code%2Cauth_avs_code_raw%2Cauth_response%2Cauth_amount%2Cauth_code%2Cauth_cv_result%2Cauth_cv_result_raw%2Cauth_time%2Crequest_token%2Cpayer_authentication_proof_xml%2Cpayer_authentication_reason_code%2Cpayer_authentication_enroll_e_commerce_indicator%2Cpayer_authentication_enroll_veres_enrolled%2Cscore_device_fingerprint_cookies_enabled%2Cscore_device_fingerprint_flash_enabled%2Cscore_device_fingerprint_hash%2Cscore_device_fingerprint_images_enabled%2Cscore_device_fingerprint_javascript_enabled%2Cscore_device_fingerprint_true_ipaddress%2Cscore_device_fingerprint_true_ipaddress_city%2Cscore_device_fingerprint_true_ipaddress_country%2Cscore_device_fingerprint_screen_resolution%2Cscore_device_fingerprint_smart_id%2Cscore_device_fingerprint_smart_id_confidence_level%2Cscore_device_fingerprint_browser_language%2Cscore_factors%2Cscore_rcode%2Cscore_time_local%2Cscore_model_used%2Cscore_return_code%2Cscore_host_severity%2Cscore_score_result%2Cscore_reason_code%2Cscore_rflag%2Cscore_rmsg%2Cscore_address_info%2Cscore_card_account_type%2Cscore_card_issuer%2Cscore_card_scheme%2Cscore_internet_info%2Cscore_ip_city%2Cscore_ip_country%2Cscore_ip_state%2Cscore_ip_routing_method%2Cscore_suspicious_info%2Cscore_velocity_info%2Cdecision_early_return_code%2Cdecision_early_reason_code%2Cdecision_reason_code%2Cdecision_rmsg%2Cdecision_rcode%2Cdecision_case_priority%2Cdecision_return_code%2Cdecision_early_rcode%2Cdecision_rflag%2Cdecision_velocity_info%2Cscore_bin_country%2Csigned_field_names%2Csigned_date_time&amp;decision_reason_code=100&amp;score_time_local=07%3A25%3A27&amp;req_transaction_type=authorization&amp;score_internet_info=MM-IPBC&amp;req_item_0_name=MA-1+dark+green+shell+bomber+jacket&amp;req_reference_number=350403888&amp;score_device_fingerprint_flash_enabled=false&amp;req_line_item_count=3&amp;score_device_fingerprint_images_enabled=true&amp;score_score_result=83&amp;payer_authentication_enroll_veres_enrolled=U&amp;req_item_0_unit_price=120.00&amp;payer_authentication_proof_xml=%26lt%3BAuthProof%26gt%3B%26lt%3BTime%26gt%3B2017+Aug+29+06%3A25%3A29%26lt%3B%2FTime%26gt%3B%26lt%3BDSUrl%26gt%3Bhttps%3A%2F%2Fcsrtestcustomer34.cardinalcommerce.com%2Fmerchantacsfrontend%2Fvereq.jsp%3Facqid%3DCYBS%26lt%3B%2FDSUrl%26gt%3B%26lt%3BVEReqProof%26gt%3B%26lt%3BMessage+id%3D%26quot%3BubOjWw0PgdRm7cZXuxk0%26quot%3B%26gt%3B%26lt%3BVEReq%26gt%3B%26lt%3Bversion%26gt%3B1.0.2%26lt%3B%2Fversion%26gt%3B%26lt%3Bpan%26gt%3BXXXXXXXXXXXX1111%26lt%3B%2Fpan%26gt%3B%26lt%3BMerchant%26gt%3B%26lt%3BacqBIN%26gt%3B469216%26lt%3B%2FacqBIN%26gt%3B%26lt%3BmerID%26gt%3B341422420000000%26lt%3B%2FmerID%26gt%3B%26lt%3Bpassword%26gt%3B%26lt%3B%2Fpassword%26gt%3B%26lt%3B%2FMerchant%26gt%3B%26lt%3BBrowser%26gt%3B%26lt%3BdeviceCategory%26gt%3B0%26lt%3B%2FdeviceCategory%26gt%3B%26lt%3Baccept%26gt%3B%2A%2F%2A%26lt%3B%2Faccept%26gt%3B%26lt%3BuserAgent%26gt%3BMozilla%2F5.0+%28Windows+NT+6.3%3B+Win64%3B+x64%29+AppleWebKit%2F537.36+%28KHTML%2C+like+Gecko%29+Chrome%2F59.0.3071.115+Safari%2F537.36%26lt%3B%2FuserAgent%26gt%3B%26lt%3B%2FBrowser%26gt%3B%26lt%3B%2FVEReq%26gt%3B%26lt%3B%2FMessage%26gt%3B%26lt%3B%2FVEReqProof%26gt%3B%26lt%3BVEResProof%26gt%3B%26lt%3BMessage+id%3D%26quot%3BubOjWw0PgdRm7cZXuxk0%26quot%3B%26gt%3B%26lt%3BVERes%26gt%3B%26lt%3Bversion%26gt%3B1.0.2%26lt%3B%2Fversion%26gt%3B%26lt%3BCH%26gt%3B%26lt%3Benrolled%26gt%3BU%26lt%3B%2Fenrolled%26gt%3B%26lt%3B%2FCH%26gt%3B%26lt%3Bprotocol%26gt%3BThreeDSecure%26lt%3B%2Fprotocol%26gt%3B%26lt%3B%2FVERes%26gt%3B%26lt%3B%2FMessage%26gt%3B%26lt%3B%2FVEResProof%26gt%3B%26lt%3B%2FAuthProof%26gt%3B&amp;req_card_expiry_date=02-2019&amp;score_rflag=DSCORE&amp;score_card_issuer=RIVER+VALLEY+CREDIT+UNION&amp;req_bill_to_phone=07506123456&amp;auth_response=0&amp;req_payment_method=card&amp;req_merchant_defined_data10=10&amp;score_device_fingerprint_true_ipaddress_city=southwark&amp;req_merchant_defined_data12=0&amp;req_merchant_defined_data11=27&amp;req_merchant_defined_data14=0&amp;req_merchant_defined_data13=3017&amp;req_item_1_sku=2298912&amp;payer_authentication_enroll_e_commerce_indicator=internet&amp;req_card_type=001&amp;score_device_fingerprint_screen_resolution=1920x1080&amp;score_velocity_info=VELL-TIP&amp;auth_avs_code=U&amp;score_address_info=COR-BA%5EMM-BIN&amp;score_factors=B&amp;score_model_used=default_uk&amp;decision_rmsg=Service+processed+successfully&amp;req_profile_id=AmpHn06&amp;score_device_fingerprint_hash=dd0aca37af6e422f90cf59625a6e77d6&amp;decision_rflag=SOK&amp;signed_date_time=2017-08-29T06%3A25%3A29Z&amp;req_payment_token=4974356009966100104106&amp;req_ship_to_surname=Field&amp;score_ip_state=greater+london&amp;req_ship_to_forename=John&amp;score_ip_country=gb&amp;score_card_scheme=VISA+CREDIT&amp;score_device_fingerprint_true_ipaddress_country=GB&amp;score_bin_country=US&amp;req_bill_to_address_city=London&amp;req_bill_to_address_postal_code=W11+1DG&amp;score_reason_code=100&amp;reason_code=100&amp;req_bill_to_forename=John&amp;req_device_fingerprint_id=596c84b70be20150028408721666&amp;score_card_account_type=Visa+Gold&amp;req_bill_to_email=john.field%40harveynichols.com&amp;auth_avs_code_raw=00&amp;req_currency=GBP&amp;score_device_fingerprint_smart_id_confidence_level=100.00&amp;message=Request+was+processed+successfully.&amp;req_transaction_uuid=350403888-1503987926-700463&amp;score_device_fingerprint_smart_id=dd0aca37af6e422f90cf59625a6e77d6&amp;score_return_code=1072000&amp;score_host_severity=1&amp;req_item_1_quantity=1&amp;req_access_key=7d7014ade9ea36e0b7c786829074aa3e&amp;decision_early_reason_code=100&amp;req_bill_to_address_state=N%2FA&amp;req_ship_to_address_line1=28+St+Lukes+Mews&amp;decision_early_rcode=1</Response></Cybersource></Payment></Order></PushSalesOrderRequestContainer></PushSalesOrderRequest>'''.format(str(uuid.uuid4()).replace('-', ''), order_id, group_order_id)
    
    inc_order_and_group_id()

    return headers, body

def dump_json(q, path):
    
    datalist = []
    
    while not q.empty():
        data = q.get()
        datalist.append(data)
        
    with open(path, 'wb') as f:
        json.dump(datalist, codecs.getwriter('utf-8')(f), ensure_ascii = False)
        
def dump_csv(q, path):
       
    with open(path, 'w', newline = '', encoding = 'utf-8') as f:
    
        writer = csv.writer(f)

        writer.writerow(["thread_index", "iteration", "verb", "uri", "request_headers", "request_body", "status", "response_headers", "response_content", "duration"])

        row = []

        while not q.empty():
        
            data = q.get()
            
            for key in data.keys():
                row.append(data[key])
        
            writer.writerow(row)
                
            row.clear()
                
def dump_q(q, path, format):
    if format == "csv":
        dump_csv(q, path)
    elif format == "json":
        dump_json(q, path)
             
def print_usage():
    print("Usage: test.py [-v | --verb <'post' or 'get' only right now>] [-u | --uri <the uri to get or post to>] [-t | --threads <thread count>] [-i | --iterations <iterations per thread>] [-d | --delay <delay per iteration (seconds)>] [-o | --outfile <output file>] [-f | --format <output file format (csv or json)>")
    print("Defaults are: -v get -t 1 -i 1 -d 0 -o ./results.csv -f csv")
    print("-h or --help displays this help text.")
     
def get_args(argv):

    try:
        opts, args = getopt.getopt(argv, "hv:u:t:i:d:o:f:", ["help", "verb=", "uri=", "threads=", "iterations=", "delay=", "outfile=", "format="])
        return opts, args
    except getopt.GetoptError as goe:
        print(str(goe))
        print_usage()
        sys.exit(2)

def get_function(verb):

    if verb == "get":
        func = iterate_get
    elif verb == "post":
        func = iterate_post
    else:
        print_usage()
        sys.exit()
        
    return func

def print_params(verb, uri, thread_count, iterations_per_thread, 
                 delay_per_iteration, outfile, outfile_format):

    print("verb = {}".format(verb))
    print("uri = {}".format(uri))
    print("thread_count = {}".format(thread_count))
    print("iterations_per_thread = {}".format(iterations_per_thread))
    print("delay_per_iteration = {}".format(delay_per_iteration))
    print("outfile = {}".format(outfile))
    print("outfile_format = {}".format(outfile_format))
    
def go(q,
       thread_count,
       verb,
       uri, 
       iterations_per_thread, 
       delay_per_iteration):
       
    func = get_function(verb)
  
    threads = [threading.Thread(target = func, 
                                args=(q, 
                                uri, 
                                t, 
                                iterations_per_thread, 
                                delay_per_iteration)) 
               for t in range(thread_count)]

    for thread in threads:
        thread.start()
        
    for thread in threads:
        thread.join()
    
def main(argv):   

    verb = "post"
    #uri = "http://localhost:24589/api/order"
    uri = 'http://localhost:24589/api/submit'
    thread_count = 1
    iterations_per_thread = 1
    delay_per_iteration = 0
    outfile = "./results.csv"
    outfile_format = "csv"

    opts, args = get_args(argv)

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            print_usage()
            sys.exit()
        elif opt in ("-v", "--verb"):
            verb = arg.lower()
        elif opt in ("-u", "--uri"):
            uri = arg
        elif opt in ("-t", "--threads"):
            thread_count = int(arg)
        elif opt in ("-i", "--iterations"):
            iterations_per_thread = int(arg)
        elif opt in ("-d", "--delay"):
            delay_per_iteration = int(arg)
        elif opt in ("-o", "--outfile"):
            outfile = arg
        elif opt in ("-f", "--format"):
            outfile_format = arg.lower()

    if uri is None:
        uri = "http://example.com"
        print("uri is a required param but has been defaulted to http://example.com")
        verb = 'get'
        print("and verb is defaulted to get!")

    print_params(verb, uri, thread_count, iterations_per_thread, 
                 delay_per_iteration, outfile, outfile_format)

    q = queue.Queue()
    
    go(q,
       thread_count,
       verb,
       uri, 
       iterations_per_thread, 
       delay_per_iteration)

    print("saving...")
    
    dump_q(q, outfile, outfile_format)
        
    print("Done.")
        
if __name__ == "__main__":
    main(sys.argv[1:])
