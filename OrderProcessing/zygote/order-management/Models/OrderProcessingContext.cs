﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace OrderManagement.Models
{
    public partial class OrderProcessingContext : DbContext
    {
        private IConfiguration _config;

        public virtual DbSet<OrderCurrentState> OrderCurrentState { get; set; }
        public virtual DbSet<OrderProcessingState> OrderProcessingState { get; set; }
        public virtual DbSet<OrderState> OrderState { get; set; }

        public virtual DbSet<OrderStateTotalPayload> OrderStaeTotal { get; set;}

        public OrderProcessingContext(IConfiguration config)
        {
            _config = config;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder
                    .UseSqlServer
                    (_config.GetConnectionString("orderProcessing"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OrderCurrentState>(entity =>
            {
                entity.Property(e => e.Created)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.OrderState).HasDefaultValueSql("((0))");

                entity.Property(e => e.OrderStateDescription).IsRequired();

                entity.Property(e => e.ProcessingState).HasDefaultValueSql("((0))");

                entity.Property(e => e.ProcessingStateDescription).IsRequired();

                entity.Property(e => e.Updated)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getutcdate())");

                entity.Property(e => e.Xml).HasColumnType("xml");
            });

            modelBuilder.Entity<OrderProcessingState>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<OrderState>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });
        }
    }
    
    public class OrderStatePayload
    {
        public long OrderId { get; set; }
        public OrderStateEnum OrderState { get; set; }
        public string OrderStateDescription { get; set; }
        public OrderProcessingStateEnum ProcessingState { get; set; }
        public string ProcessingStateDescription { get; set; }
        public string Comment { get; set; }
    }

    public class OrderStateTotalPayload
    {
        public OrderStateEnum State { get; set; }
        public string Description { get; set; }
        public long Count { get; set; }
    }
}
