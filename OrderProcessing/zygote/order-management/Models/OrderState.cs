﻿using System;
using System.Collections.Generic;

namespace OrderManagement.Models
{
    public partial class OrderState
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public int Value { get; set; }
    }
}
