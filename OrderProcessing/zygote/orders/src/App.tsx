import * as React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import OrderStatusList from './OrderStatusList';

const App = () => {
    return (
      <div>
        <OrderStatusList />
      </div>
    );
};

export default App;
