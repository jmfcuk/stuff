export interface OrderStatusDef {
    
    orderId: number;
    orderState: OrderStateEnum;
    orderStateDescription: string;
    processingState: OrderProcessingStateEnum;
    processingStateDescription: string;
    comment: string;
};

export enum OrderStateEnum {
    
    unknown = -999,
    pending = 0,
    importing = 1,
    imported = 2,
    assigning = 3,
    assigned = 4,
    picking = 5,
    picked = 6,
    packing = 7,
    packed = 8,
    dispatching = 9,
    dispatched = 10,
    refunding = 11,
    refunded = 12,
    cancelling = 13,
    cancelled = 14,
    complete = 15,
}

export enum OrderProcessingStateEnum {
    
    unknown = -999,
	pending = 0,
	processing = 1,
	success = 2,
	warning = 3,
	error = 4,
}

export const StateColorMap = (): Map<OrderStateEnum, string> => {

    let m = new Map<OrderStateEnum, string>();

    m[OrderStateEnum.unknown] = '';
    m[OrderStateEnum.pending] = '';
    m[OrderStateEnum.importing] = '';
    m[OrderStateEnum.imported] = '';
    m[OrderStateEnum.assigning] = '';
    m[OrderStateEnum.assigned] = '';
    m[OrderStateEnum.picking] = '';
    m[OrderStateEnum.picked] = '';
    m[OrderStateEnum.packed] = '';
    m[OrderStateEnum.dispatching] = '';
    m[OrderStateEnum.dispatched] = '';
    m[OrderStateEnum.cancelling] = '';
    m[OrderStateEnum.cancelled] = '';
    m[OrderStateEnum.complete] = '';

    return m;
}

export default OrderStatusDef;
