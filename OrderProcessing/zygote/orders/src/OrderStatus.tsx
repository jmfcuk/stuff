import * as React from 'react';
import OrderStatusDef from './OrderStatus.d';
import { Row, Col } from 'react-bootstrap';

interface OrderStatusProps {
    orderStatus: OrderStatusDef;
}

class OrderStatus extends React.Component<OrderStatusProps> {

    constructor(props: OrderStatusProps) {
        super(props);

    }

    render() {
        return (
            <Row>
                <Col xs={1}><div>{this.props.orderStatus.orderId}</div></Col>
                <Col xs={4}>
                    <span>
                    {this.props.orderStatus.orderState + ' - '}
                    {this.props.orderStatus.orderStateDescription}
                    </span>
                </Col>
                <Col xs={4}>
                    <span>
                    {this.props.orderStatus.processingState + ' - '}
                    {this.props.orderStatus.processingStateDescription}
                    </span>
                </Col>
                <Col xs={3}><div>{this.props.orderStatus.comment}</div></Col>
            </Row>
        );
    }
}

export default OrderStatus;
