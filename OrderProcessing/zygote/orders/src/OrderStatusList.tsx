import * as React from 'react';
import { Grid, Row, Col } from 'react-bootstrap';
import OrderStatusDef from './OrderStatus.d';
import OrderStatus from './OrderStatus';

interface OrderStatusListState {
    orders: Array<OrderStatusDef>;
}

class OrderStatusList extends React.Component<any, OrderStatusListState> {

    state: OrderStatusListState;

    constructor(props: any) {

        super(props);

        this.state = {
             orders: []
        };
    }

    componentDidMount() {

        const url = 'http://localhost:24589/api/management/state';

        const init: any = {
          mode: 'cors',
        };
  
        fetch(url, init)
          .then(response => response.json())
          .then(data => { this.setState({orders: data}); });
    }

    render(): JSX.Element {
        return (
            <Grid>
                <Row>
                    <Col xs={1}>Id</Col>
                    <Col xs={4}>Order State</Col>
                    <Col xs={4}>Processing State</Col>
                    <Col xs={3}>Comment</Col>
                </Row>
                
                    {this.renderOrders()}
                
            </Grid>
        );
    }

    renderOrders = (): Array<JSX.Element> => {
        return this.state.orders.map((order: OrderStatusDef) => (
            <Row>
                <OrderStatus key={order.orderId}
                            orderStatus={order} 
                />
            </Row>
        ));
    }
}

export default OrderStatusList;
